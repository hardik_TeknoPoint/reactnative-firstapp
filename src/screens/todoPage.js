import React, { Component } from "react";
import { View, Text } from "react-native";
import TodoMain from "../components/todoComponents/todoMain";
import BottomNav from "../components/bottomNav";
export default class TodoPage extends Component {
  render() {
    return (
      <View>
        <TodoMain />
        <BottomNav navigate={this.props.navigation.navigate} />
      </View>
    );
  }
}
