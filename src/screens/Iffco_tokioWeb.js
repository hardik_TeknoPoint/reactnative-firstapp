//import React in our code
// import React from "react";
//import all the components we are going to use
import React, { Component, useRef } from "react";
import { SafeAreaView } from "react-native";
import { WebView } from "react-native-webview";
import PageNotFound from "./pageNotFound";
// const myHtml =
{
  /* <img src="../assets/images/Tekno-point.png"/> */
}
// document.querySelector(".logoContainer").innerHTML;
//   document.querySelector('.logoContainer').style.display = 'none'
// document.querySelector('.logoContainer').innerHTML = "<div><h1>Its working</h1></div>";
// C:\Users\Hardik.Mistry\Desktop\xd demo\chinmy-iffco_tokio\html-task\Assets
const InjectedJavaScript = `(function () {
    document.querySelector('.logoContainer').innerHTML = "<button style='background-color:red;padding:20px;' onclick='(function(){window.ReactNativeWebView.postMessage(JSON.stringify(window.location))})()'> click To fetch Data.</button>";
    document.querySelector('.formHeading').style.color='black';
    
})()`;

export default class IffcoTokioWeb extends Component {
  state = {
    showNotFoundPage: false,
    mobileViewData: {},
  };
  //   webRef = useRef();
  msgFromWebView = (syntheticEvent) => {
    const { nativeEvent } = syntheticEvent;
    // const myData = { ...nativeEvent };
    const myData = Object.entries(nativeEvent);
    console.log("logging from webView post message.", myData);
    alert(myData);
    this.setState({ mobileViewData: myData });
    console.log();
    console.log();
    console.log();
    console.log();
    console.log("data from state ===--", this.state.mobileViewData);
  };
  render() {
    return (
      <>
        {!this.state.showNotFoundPage ? (
          <SafeAreaView style={{ flex: 1 }}>
            <WebView
              source={{ uri: "http://192.168.11.13:5500/index.html" }}
              //   source={{ html: "<h1>Hello world</h1>" }}
              style={{ flex: 1, marginTop: 20 }}
              // injectedJavaScriptBeforeContentLoaded={InjectedJavaScript}
              injectedJavaScript={InjectedJavaScript}
              onError={({ nativeEvent }) => {
                console.log("there is some error occured ===---", nativeEvent);
                this.setState({ showNotFoundPage: true });
              }}
              onMessage={this.msgFromWebView}
            />
          </SafeAreaView>
        ) : (
          <PageNotFound onPress={this.props.navigation.goBack} />
        )}
      </>
    );
  }
}
