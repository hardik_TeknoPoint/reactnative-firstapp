import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
export default class PageNotFound extends Component {
  // goBackBtn = () => {
  //   console.log("go back btn !!");
  // };
  render() {
    return (
      <View style={style.notFoundPg}>
        <Text style={style.notFoundTxt}> Page Not Found</Text>
        <View style={style.btnContainer}>
          <TouchableOpacity
            onPress={this.props.onPress}
            style={style.gobackBtn}
            activeOpacity={0.8}
          >
            <Text style={{ fontSize: 20, fontWeight: "700" }}>Go Back</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  notFoundPg: {
    flex: 1,
    // backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
  },
  notFoundTxt: {
    fontSize: 28,
    fontWeight: "600",
  },
  btnContainer: {
    // backgroundColor: "pink",
    margin: 20,
  },
  gobackBtn: {
    backgroundColor: "#ad42db",
    paddingHorizontal: 40,
    paddingVertical: 15,
    borderRadius: 10,
  },
});
