import React, { Component } from "react";
import { View, Text } from "react-native";
import InputC from "../components/loginComponents/inputC";
export default class LoginScreen extends Component {
  render() {
    return (
      <View style={{ alignItems: "center" }}>
        <InputC navigation={this.props.navigation} route={this.props.route} />
      </View>
    );
  }
}
