import React, { Component } from "react";
import { Button, View, Text } from "react-native";
import Footer from "../components/footer";
import Header from "../components/header";
import Main from "../components/main";
// import FooterButton from "../components/footerButton";
import BottomNav from "../components/bottomNav";
// import BottomTabs from "../components/bottomTabs";

export default class HopeHome extends Component {
  // values = this.props.route.params;

  render() {
    // console.log("values==---", this.values);
    return (
      <View style={{ flex: 1 }}>
        <Header />
        <Main />
        <Footer />
        <BottomTabs />
        {/* <BottomNav navigate={this.props.navigation.navigate} /> */}
        {/* <Text>Hi</Text> */}
      </View>
    );
  }
}
