import React, { Component } from "react";
import { View, Text } from "react-native";
import FooterButton from "./footerButton";
export default class Footer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          //   backgroundColor: "red",
          marginTop: 40,
          flex: 1,
        }}
      >
        <Text
          style={{
            height: 100,
            // lineHeight: 10,
            width: "95%",
            fontWeight: "900",
            fontSize: 28,
            marginLeft: 20,
          }}
        >
          Bring Transperancy In your Treatment
        </Text>
        <FooterButton />
      </View>
    );
  }
}
