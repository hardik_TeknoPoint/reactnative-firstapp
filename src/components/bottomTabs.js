import * as React from "react";
import { View, Text, StyleSheet } from "react-native";
import { NavigationContainer, TabActions } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HopeHome from "../screens/hopeHome";
import TodoPage from "../screens/todoPage";
// function HomeScreen({ navigation }) {
//   const jumpToAction = TabActions.jumpTo("Profile", { user: "Satya" });

//   return (
//     // <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
//     //   <Text>Home!</Text>
//     //   <Button
//     //     title="Jump to Profile"
//     //     onPress={() => navigation.dispatch(jumpToAction)}
//     //   />
//     // </View>
//     <View style={style.bottomBtn}>
//       <Pencil
//         name="home"
//         size={25}
//         color="#6b32a8"
//         // onPress={() => this.props.navigate("HopeHome")}
//       />
//       <Text style={style.bottomBtnText}>Todo</Text>
//     </View>
//   );
// }

// function TodoPage({ route }) {
//   return (
//     // <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
//     //   <Text>Profile!</Text>
//     //   <Text>{route?.params?.user ? route.params.user : "Noone"}'s profile</Text>
//     // </View>
//     <View style={style.bottomBtn}>
//       <Pencil
//         name="pencil"
//         size={25}
//         color="#6b32a8"
//         // onPress={() => this.props.navigate("TodoPage")}
//       />
//       <Text style={style.bottomBtnText}>Todo</Text>
//     </View>
//   );
// }

const Tab = createBottomTabNavigator();

export default function BottomTabs() {
  return (
    // <NavigationContainer>
    <Tab.Navigator>
      {/* <Tab.Screen name="Home" component={HomeScreen} /> */}
      <Tab.Screen name="Home" component={HopeHome} />
      <Tab.Screen name="Profile" component={TodoPage} />
    </Tab.Navigator>
    // </NavigationContainer>
  );
}

const style = StyleSheet.create({
  BottomNav: {
    width: "100%",
    height: 60,
    backgroundColor: "#d5d5d5",
    justifyContent: "space-around",
    flexDirection: "row",
    // position: "absolute",
    // bottom: 0,
  },
  bottomBtn: {
    justifyContent: "center",
    alignItems: "center",
    width: 60,
    height: 60,
  },
  bottomBtnText: {
    fontWeight: "600",
    fontSize: 14,
  },
});
