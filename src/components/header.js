import React, { Component } from "react";
import { View, Text, Image } from "react-native";

export default class Header extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          width: 200,
          height: 50,
          marginLeft: "5%",
          marginTop: 20,
          //   backgroundColor: "red",
        }}
      >
        <Image
          source={require("../assets/images/logo.png")}
          resizeMode="contain"
          style={{ width: 180, height: 50 }}
        />
      </View>
    );
  }
}
