import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
// import { Carousel } from "react-native-snap-carousel";

// render () {
//   return (

//   );
// }
// }
export default class MyCarousel extends Component {
  state = {
    entries: [
      { title: "1" },
      { title: "2" },
      { title: "3" },
      { title: "4" },
      { title: "5" },
      { title: "6" },
      { title: "7" },
    ],
  };
  _renderItem = ({ item, index }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
    );
  };
  render() {
    return (
      // <View>
      //   <Text>My Carousel component !!</Text>
      // </View>
      <Carousel
        // ref={(c) => {
        //   this._carousel = c;
        // }}
        data={this.state.entries}
        renderItem={this._renderItem}
        // sliderWidth={sliderWidth}
        // itemWidth={itemWidth}
      />
    );
  }
}

const styles = StyleSheet.create({
  slide: {
    backgroundColor: "red",
  },
  title: {
    backgroundColor: "green",
  },
});
