import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Pencil from "react-native-vector-icons/Entypo";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class BottomNav extends Component {
  render() {
    return (
      <View style={style.BottomNav}>
        <View style={style.bottomBtn}>
          <Pencil
            name="home"
            size={25}
            color="#6b32a8"
            onPress={() => this.props.navigate("HopeHome")}
          />
          <Text style={style.bottomBtnText}>Todo</Text>
        </View>
        <View style={style.bottomBtn}>
          <Pencil
            name="pencil"
            size={25}
            color="#6b32a8"
            onPress={() => this.props.navigate("TodoPage")}
          />
          <Text style={style.bottomBtnText}>Todo</Text>
        </View>
        <View style={style.bottomBtn}>
          <Icon
            name="logout"
            size={25}
            color="#6b32a8"
            // onPress={() => this.props.navigate("LoginScreen")}
            onPress={() => this.props.navigate("IffcoTokioWeb")}
          />
          {/* <Text style={style.bottomBtnText}>Todo</Text> */}
          <Text style={style.bottomBtnText}>IffcoTokioWeb</Text>
        </View>
      </View>
    );
  }
}
const style = StyleSheet.create({
  BottomNav: {
    width: "100%",
    height: 60,
    backgroundColor: "#d5d5d5",
    justifyContent: "space-around",
    flexDirection: "row",
    // position: "absolute",
    // bottom: 0,
  },
  bottomBtn: {
    justifyContent: "center",
    alignItems: "center",
    width: 60,
    height: 60,
  },
  bottomBtnText: {
    fontWeight: "600",
    fontSize: 14,
  },
});
