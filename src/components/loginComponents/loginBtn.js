import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";

const TouchableOpacityStyle = {
  backgroundColor: "#6b32a8",
  borderRadius: 5,
  width: 300,
  height: 50,
  alignItems: "center",
  display: "flex",
  justifyContent: "center",
  marginTop: 60,
};
const registerBtnStyle = {
  borderRadius: 5,
  alignItems: "center",
  display: "flex",
  justifyContent: "center",
  marginLeft: 8,
};
export default class LoginBtn extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <View>
          <TouchableOpacity
            onPress={() => {
              alert("clicked");
            }}
            activeOpacity={0.8}
            style={TouchableOpacityStyle}
          >
            <Text>Login</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row", marginTop: 30 }}>
          <Text>Don't have account ?</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigate("HopeHome", { name: "hardik" });
            }}
            activeOpacity={0.8}
            style={registerBtnStyle}
          >
            <Text style={{ color: "#6b32a8", fontWeight: "600" }}>
              Register Here.
            </Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
