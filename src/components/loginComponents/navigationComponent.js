import React, { Component } from "react";
import { View, Text, Button } from "react-native";
// import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
// import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createStackNavigator } from "@react-navigation/stack";

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Home Screen</Text>
    </View>
  );
}
function Bazooka() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Bazooka Screen</Text>
    </View>
  );
}

const Stack = createStackNavigator();
export default class NavigationComponent extends Component {
  render() {
    return (
      <View>
        <NavigationContainer>
          <Stack.Navigator initialRouteName={"HomeScreen"}>
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{
                title: "First Page", //Set Header Title
                headerStyle: {
                  backgroundColor: "#f4511e", //Set Header color
                },
                headerTintColor: "#fff", //Set Header text color
                headerTitleStyle: {
                  fontWeight: "bold", //Set Header text style
                },
              }}
            />
            {/* <Stack.Screen name="bazooka" component={<Bazooka />} /> */}
          </Stack.Navigator>
        </NavigationContainer>
        {/* // <Button title="Home" onPress={this.log} /> */}
      </View>
    );
  }
}
