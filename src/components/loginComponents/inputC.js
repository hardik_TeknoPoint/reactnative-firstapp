import React, { Component } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from "react-native";
import Icon from "react-native-vector-icons/Fontisto";
import Lock from "react-native-vector-icons/Entypo";
import BottomLink from "./bottomLink";
import FormBtn from "./formBtn";

export default class InputC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailValue: "",
      password: "",
      phoneNo: "",
    };
  }

  setEmailValue = (emailValue) => {
    this.setState({ emailValue });
  };
  setPassword = (password) => {
    this.setState({ password });
  };
  setPhoneNo = (phoneNo) => {
    this.setState({ phoneNo });
  };

  loginBtn = () => {
    // const values = this.props.route.params;
    console.log("data in state ==--", this.state.rgEmail, this.state.rgPass);
    if (
      this.state.emailValue === this.props.route.params?.email &&
      this.state.password === this.props.route.params?.password
    ) {
      console.log("login success full !!");
      this.props.navigation.navigate("HopeHome");
    } else {
      console.log("fail !!!");
      alert("login Fail !");
    }
  };
  render() {
    return (
      <>
        <View style={{ marginTop: 100 }}>
          <Image
            source={require("../../assets/images/Tekno-point.png")}
            resizeMode="contain"
            style={{ width: 300 }}
          />
        </View>
        <View style={style.flexRow}>
          <Icon name="email" size={20} />
          <TextInput
            style={{ width: "90%", paddingLeft: 10 }}
            onChangeText={this.setEmailValue}
            placeholder="Enter Email Adress"
          />
        </View>

        <View style={style.flexRow}>
          <Lock name="lock" size={20} />
          <TextInput
            style={{ width: "90%", paddingLeft: 10 }}
            onChangeText={this.setPassword}
            placeholder="Enter Password"
          />
        </View>
        <FormBtn
          btnName={"Login"}
          onPress={() => {
            this.loginBtn();
          }}
        />
        <BottomLink
          question={"Don't have account ?"}
          redirctor={"Register Here."}
          onPress={() => {
            this.props.navigation.navigate("registerPage", {
              name: "hardik",
            });
          }}
        />
      </>
    );
  }
}

const style = StyleSheet.create({
  flexRow: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#e5e5e5",
    width: "80%",
    margin: 10,
    marginTop: 20,
    justifyContent: "space-around",
  },
  TouchableOpacityStyle: {
    backgroundColor: "#6b32a8",
    borderRadius: 5,
    width: 300,
    height: 50,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginTop: 40,
  },
  TouchableOpacityStyle: {
    backgroundColor: "#6b32a8",
    borderRadius: 5,
    width: 300,
    height: 50,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginTop: 60,
  },
  registerBtnStyle: {
    borderRadius: 5,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginLeft: 8,
  },
});
