import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
export default class FormBtn extends Component {
  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={this.props.onPress}
          activeOpacity={0.8}
          style={style.TouchableOpacityStyle}
        >
          {/* <Text>Register</Text> */}
          <Text>{this.props.btnName}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const style = StyleSheet.create({
  TouchableOpacityStyle: {
    backgroundColor: "#6b32a8",
    borderRadius: 5,
    width: 300,
    height: 50,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginTop: 60,
  },
});
