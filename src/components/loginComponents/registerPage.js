import React, { Component } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  // TouchableOpacity,
  // Text,
  Image,
} from "react-native";
import Icon from "react-native-vector-icons/Fontisto";
import Lock from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
// import LoginBtn from "./loginBtn";
// import Register from "./register";
import BottomLink from "./bottomLink";
import FormBtn from "./formBtn";
export default class RegisterPage extends Component {
  state = {
    emailValue: "",
    password: "",
    phoneNo: "",
  };
  setEmailValue = (emailValue) => {
    this.setState({ emailValue });
  };
  setPassword = (password) => {
    this.setState({ password });
  };
  setPhoneNo = (phoneNo) => {
    this.setState({ phoneNo });
  };
  showAlert = () => {
    alert([this.state.emailValue, this.state.password, this.state.phoneNo]);
  };
  render() {
    return (
      <View style={{ alignItems: "center" }}>
        <View style={{ marginTop: 100 }}>
          <Image
            source={require("../../assets/images/Tekno-point.png")}
            resizeMode="contain"
            style={{ width: 300 }}
          />
        </View>
        <View style={style.flexRow}>
          <Icon name="email" size={20} />
          <TextInput
            style={{ width: "90%", paddingLeft: 10 }}
            onChangeText={this.setEmailValue}
            placeholder="Enter User Email Adress"
          />
        </View>
        <View style={style.flexRow}>
          <Lock name="lock" size={20} />
          <TextInput
            style={{ width: "90%", paddingLeft: 10 }}
            onChangeText={this.setPassword}
            placeholder="Enter User Email Adress"
          />
        </View>
        <View style={style.flexRow}>
          <Feather name="phone-call" size={20} />
          <TextInput
            style={{ width: "90%", paddingLeft: 10 }}
            onChangeText={this.setPhoneNo}
            placeholder="Enter User Email Adress"
          />
        </View>
        <FormBtn
          onPress={() => {
            this.props.navigation.navigate("LoginScreen", {
              email: this.state.emailValue,
              password: this.state.password,
              phoneNo: this.state.phoneNo,
            });
          }}
          btnName={"Register"}
        />
        <BottomLink
          question={"Already Have Account ?"}
          redirctor={"Login Here"}
          onPress={() => {
            this.props.navigation.goBack();
          }}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  flexRow: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#e5e5e5",
    width: "80%",
    margin: 10,
    marginTop: 20,
    justifyContent: "space-around",
  },
  TouchableOpacityStyle: {
    backgroundColor: "#6b32a8",
    borderRadius: 5,
    width: 300,
    height: 50,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginTop: 40,
  },
  registerBtnStyle: {
    borderRadius: 5,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginLeft: 8,
  },
});
