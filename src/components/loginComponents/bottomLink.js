import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
export default class BottomLink extends Component {
  render() {
    return (
      <View style={{ flexDirection: "row", marginTop: 30 }}>
        <Text>{this.props.question}</Text>
        <TouchableOpacity
          onPress={this.props.onPress}
          activeOpacity={0.8}
          style={style.registerBtnStyle}
        >
          <Text style={{ color: "#6b32a8", fontWeight: "600" }}>
            {this.props.redirctor}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const style = StyleSheet.create({
  registerBtnStyle: {
    borderRadius: 5,
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    marginLeft: 8,
  },
});
