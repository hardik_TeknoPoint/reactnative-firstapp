import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import TodoInput from "./todoInput";
import TodoList from "./todoList";
export default class TodoMain extends Component {
  state = {
    tasks: [
      // { taskName: "default", taskId: "", complated: false },
      // { taskName: "1", taskId: "", complated: false },
      // { taskName: "2", taskId: "", complated: false },
      // { taskName: "3", taskId: "", complated: false },
    ],
  };

  addtask = (textValue) => {
    this.setState((pre) => {
      console.log("pre===", pre);
      return {
        tasks: [
          ...pre.tasks,
          {
            taskName: textValue,
            taskId: Math.round(Math.random() * 1000),
            complated: false,
          },
        ],
      };
    });
  };
  deleteTask = (taskId) => {
    console.log(taskId);
    const newArr = this.state.tasks.filter((value, i) => {
      return i !== taskId;
    });
    this.setState({ tasks: newArr });
  };
  makeComplated = (taskId) => {
    const newArr = this.state.tasks.map((value, i) => {
      if (i === taskId) {
        return { ...value, complated: !value.complated };
      }
      return value;
    });
    console.log("complated ==", newArr);
    this.setState({ tasks: newArr });
  };
  render() {
    return (
      <View style={style.todoPageContainer}>
        <Text style={style.mainHeading}>Todo Application</Text>
        <TodoInput flexRow={style.flexRow} addtask={this.addtask} />
        <TodoList
          tasks={this.state.tasks}
          deleteTask={this.deleteTask}
          makeComplated={this.makeComplated}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  todoPageContainer: {
    padding: 15,
    alignItems: "center",
  },
  mainHeading: {
    color: "black",
    fontSize: 24,
    fontWeight: "700",
    textAlign: "center",
    // backgroundColor: "pink",
  },
  flexRow: {
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    borderWidth: 2,
    borderColor: "#e5e5e5",
    borderRadius: 5,
    width: "90%",
    marginTop: 40,
    flexDirection: "row",
  },
});
