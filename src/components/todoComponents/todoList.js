import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

const dataArr = [
  { name: "a", id: 1 },
  { name: "b", id: 2 },
  { name: "c", id: 3 },
  { name: "d", id: 4 },
  { name: "e", id: 5 },
  { name: "f", id: 6 },
  { name: "g", id: 7 },
  { name: "h", id: 8 },
  { name: "i", id: 9 },
  { name: "j", id: 10 },
  { name: "k", id: 11 },
  { name: "l", id: 12 },
  { name: "m", id: 13 },
  { name: "n", id: 14 },
  { name: "o", id: 15 },
  { name: "p", id: 16 },
  { name: "q", id: 17 },
  { name: "r", id: 18 },
  { name: "s", id: 19 },
  { name: "t", id: 20 },
  { name: "v", id: 21 },
  { name: "u", id: 22 },
  { name: "w", id: 23 },
  { name: "x", id: 24 },
  { name: "y", id: 25 },
  { name: "z", id: 26 },
];

export default class TodoList extends Component {
  checkUpadated = (item) => {
    return !item.complated ? style.listTask : style.complatedTask;
  };
  listTodo = ({ item, index }) => {
    // console.log("item==", item, "index====", index);
    // console.log(
    //   "item.complated",
    //   item.complated ? style.listTask : style.complatedTask
    // );
    return (
      <TouchableOpacity
        style={this.checkUpadated(item)}
        key={index}
        onPress={() => this.props.makeComplated(index)}
        activeOpacity={0.8}
      >
        <Text style={style.listText}>{item.taskName}</Text>
        <Icon
          name="delete"
          size={25}
          color={"red"}
          onPress={() => this.props.deleteTask(index)}
        />
      </TouchableOpacity>
    );
  };
  render() {
    console.log("this.props.tasks.length---------", this.props.tasks.length);
    return (
      <View style={style.todoList}>
        <Text style={style.listHeading}>todo List </Text>
        {this.props.tasks.length !== 0 ? (
          <FlatList
            //   data={dataArr}
            data={this.props.tasks}
            renderItem={this.listTodo}
            style={{ height: "75%" }}
          />
        ) : (
          <View style={style.emptyBlock}>
            <Icon name="inbox" size={50} style={style.boxIcon} />
            <Text style={style.emptyText}>Your Task List Is Empty.</Text>
          </View>
        )}
      </View>
    );
  }
}

const style = StyleSheet.create({
  todoList: {
    width: "106%",
    marginTop: 20,
    // padding: 0,
    backgroundColor: "red",
    height: 455,
  },
  listText: {
    fontSize: 20,
    fontWeight: "500",
    textAlign: "center",
  },
  listHeading: {
    width: "100%",
    marginVertical: 10,
    padding: 7,
    backgroundColor: "#f0f0f0",
    fontSize: 25,
    fontWeight: "700",
    textAlign: "center",
  },
  listTask: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginTop: 10,
    padding: 15,
    borderBottomColor: "lightgrey",
    borderBottomWidth: 2,
    backgroundColor: "#f0f0f0",
  },
  emptyBlock: {
    width: "100%",
    height: 300,
    justifyContent: "center",
    alignItems: "center",
  },
  boxIcon: {
    marginBottom: 10,
    shadowColor: "#000",
  },
  emptyText: {
    fontSize: 20,
    fontWeight: "600",
  },
  complatedTask: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginTop: 10,
    padding: 15,
    borderBottomColor: "lightgrey",
    borderBottomWidth: 2,
    backgroundColor: "lightgreen",
  },
});
