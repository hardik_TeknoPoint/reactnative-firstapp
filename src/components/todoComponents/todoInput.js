import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

export default class TodoInput extends Component {
  state = {
    textValue: "",
    err: false,
  };

  addtask = () => {
    if (this.state.textValue.trim() === "") {
      return this.setState({ err: true });
    } else {
      this.setState({ err: false });
    }
    this.props.addtask(this.state.textValue);
    this.setState({ textValue: "" });
  };
  setTextValue = (textValue) => {
    this.setState({ textValue });
  };
  render() {
    return (
      <>
        <View style={this.props.flexRow}>
          <TextInput
            style={style.todoInput}
            value={this.state.textValue}
            onChangeText={this.setTextValue}
            placeholder="Enter Your Task Here"
          />
          <TouchableOpacity onPress={this.addtask} style={style.addTaskBtn}>
            <Text style={{ fontWeight: "700" }}>add Task</Text>
          </TouchableOpacity>
        </View>
        <View style={{ height: 20 }}>
          {this.state.err && this.state.textValue.trim() == "" ? (
            <Text style={{ color: "red" }}>
              please Enter some value in text box
            </Text>
          ) : null}
        </View>
      </>
    );
  }
}

const style = StyleSheet.create({
  todoInput: {
    width: "75%",
    fontSize: 20,
  },
  addTaskBtn: {
    backgroundColor: "#e5e5e5",
    padding: 10,
    borderRadius: 5,
  },
});
