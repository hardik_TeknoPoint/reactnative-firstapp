import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";

const TouchableOpacityStyle = {
  backgroundColor: "#6b32a8",
  borderRadius: 5,
  width: 300,
  height: 50,
  alignItems: "center",
  display: "flex",
  justifyContent: "center",
};
export default class FooterButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          marginLeft: 40,
        }}
      >
        <TouchableOpacity
          onPress={() => {
            alert("clicked");
          }}
          activeOpacity={0.8}
          style={TouchableOpacityStyle}
        >
          <Text style={{ fontSize: 18, marginRight: 40 }}>Get Staggggrted</Text>
          <Image
            source={require("../assets/images/rigthArrow.png")}
            resizeMode="contain"
            style={{
              width: 30,
              height: 50,
              position: "absolute",
              left: "70%",
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
