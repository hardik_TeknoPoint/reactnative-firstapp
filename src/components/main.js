import React, { Component } from "react";
import { View, Image } from "react-native";
export default class Main extends Component {
  render() {
    return (
      <View
        style={{
          marginTop: 80,
          width: 350,
          height: 350,
          marginHorizontal: 20,
        }}
      >
        <Image
          resizeMode="contain"
          source={{ uri: "https://freesvg.org/img/1531595967.png" }}
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </View>
    );
  }
}
