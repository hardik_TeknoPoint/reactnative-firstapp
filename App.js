import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import HopeHome from "./src/screens/hopeHome";
import LoginScreen from "./src/screens/loginScreen";
import TodoPage from "./src/screens/todoPage";
import RegisterPage from "./src/components/loginComponents/registerPage";
import CarouselScreen from "./src/screens/carouselScreen";
import IffcoTokioWeb from "./src/screens/Iffco_tokioWeb";
import PageNotFound from "./src/screens/pageNotFound";
// import BottomTabs from "./src/components/bottomTabs";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
// import { createNativeStackNavigator } from "@react-navigation/native-stack";
// import { enableScreens } from "react-native-screens";
// enableScreens(true);

// const Stack = createNativeStackNavigator();

export default function App() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      {/* <StatusBar backgroundColor="red" /> */}
      {/* <HopeHome /> */}
      {/* <TodoPage /> */}
      {/* <LoginScreen /> */}
      {/* <NavigationComponent /> */}

      <Stack.Navigator initialRouteName={"LoginScreen"}>
        {/* <Stack.Navigator initialRouteName={"PageNotFound"}> */}
        {/* <Stack.Navigator initialRouteName={"CarouselScreen"}> */}
        <Stack.Screen
          name="HopeHome"
          component={HopeHome}
          // component={<BottomTabs />}
          options={{
            headerMode: false,
            title: "HopeHome", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{
            headerMode: false,
            title: "Login Page", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="TodoPage"
          component={TodoPage}
          options={{
            headerMode: false,
            title: "TodoPage", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="registerPage"
          component={RegisterPage}
          options={{
            headerMode: false,
            title: "Register Page", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="CarouselScreen"
          component={CarouselScreen}
          options={{
            headerMode: false,
            title: "CarouselScreen Page", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="IffcoTokioWeb"
          component={IffcoTokioWeb}
          options={{
            headerMode: false,
            title: "IffcoTokioWeb", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
        <Stack.Screen
          name="PageNotFound"
          component={PageNotFound}
          options={{
            headerMode: false,
            title: "PageNotFound", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
    // backgroundColor: "red",
  },
});
